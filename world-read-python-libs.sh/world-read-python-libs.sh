#!/bin/sh
worldreadpythonlibs_version="2018-04-06a"
for word in /usr/lib{,64}/python2.7/site-packages ;
do
   find ${word} -exec chmod g+rX,o+rX {} \;
done