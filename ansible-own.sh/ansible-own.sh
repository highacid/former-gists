#!/bin/sh
ansibleown_version="2018-04-04a"
tu=ansible
tg="$( id -ng "${tu}" )"
for word in $@ ;
do
   # set group accessible 
   find ${word} -exec chown "${tu}:${tg}" {} \; -exec chmod g+rwX {} \;
   # set setgid and sticky bits
   find ${word} -type d -exec chmod g+s,o+t {} \;
done
