#!/bin/sh
# https://stackoverflow.com/questions/20359936/import-an-existing-git-project-into-gitlab/30483494#30483494

username="bgstack15"
repo="${1}"

func() {
   git clone --mirror "https://github.com/${username}/${repo}" "./${repo}"
   pushd "${repo}"
   git remote add gitlab "https://gitlab.com/${username}/${repo}.git"
   git push gitlab --mirror
   popd
}

time func ;

#Now if you have a locally cloned repository that you want to keep using with the new remote, just run the following commands* there:
#
#git remote remove origin
#git remote add origin "http://gitlab.example.com/${username}/${repo}.git"
#git fetch --all

