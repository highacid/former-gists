# Filename: shelldiag.sh
# License: CC-BY-SA 4.0
# Author: bgstack15
# Startdate: 2018-08-14
# Title: Script That Demonstrates Shell Diagnostics
# Purpose: To show how to use shell built-ins and shell-independent operations to provide value in other scripts
# History:
# Usage:
# Reference:
#    man bash ksh
#    framework.sh 2017-11-11a
#    zsh filename https://stackoverflow.com/questions/9901210/bash-source0-equivalent-in-zsh/23259585#23259585
# Improve:
# Documentation:
# tested on ksh93 and bash4

detect_os() {
   # call: detect_os MYOS MYFLAVOR MYFLAVORVERSION
   __myos="${1:-MYOS}"
   __myflavor="${2:-MYFLAVOR}"
   __myflavorversion="${3:-MYFLAVORVERSION}"
   __thisflavor=""
   __thisflavorversion=""
   eval export ${__myos}=\"$( uname -s )\"

   # get __thisflavor and __thisflavorversion. Examples: centos, ubuntu, redhat
   if test -f /etc/os-release;
   then
      eval __thisflavor=$( grep -iE "^\s*ID=" /etc/os-release 2>/dev/null | sed 's/^.*=//;' | tr 'A-Z' 'a-z' )
      eval __thisflavorversion=$( grep -iE "^\s*PRETTY_NAME=" /etc/os-release 2>/dev/null | sed -e 's/^.*=//;' | tr -dc '0-9.' )
   elif test -f /etc/system-release && test $( wc -l < /etc/system-release 2>/dev/null ) -eq 1;
   then
      eval __thisflavor=$( awk '{print $1}' < /etc/system-release 2>/dev/null | tr 'A-Z' 'a-z' )
      eval __thisflavorversion=$( </etc/system-release sed -e 's/^.*=//;' 2>/dev/null | tr -dc '0-9.' )
   else
      if test "${__myos}" = "FreeBSD"; then
         __thisflavor="$( uname -i )"; __thisflavorversion="$( uname -r )";
      else
         __thisflavor="other"
         __thisflavorversion="unknown"
      fi
   fi

   eval export ${__myflavor}=\"${__thisflavor}\"
   eval export ${__myflavorversion}=\"${__thisflavorversion}\"

}

detect_shell() {
   # call: detect_shell MYSHELL MYSOURCED MYSCRIPT MYFUNCTION
   __myshell="${1:-MYSHELL}"
   __mysourced="${2:-MYSOURCED}"
   __myscript="${3:-MYSCRIPT}"
   __myfunction="${4:-MYFUNCTION}"

   case "$( eval echo \"\$${__myos}\" )" in
      Linux)
         eval export ${__myshell}=\"$( readlink -f /proc/$$/exe )\"
         ;;
      *)
         # this works on linux, but Linux has the /proc filesystem
         eval export ${__myshell}=\"$( ps -p $$ -oargs= | awk '{print $1}' | xargs which | xargs readlink -f )\"
         ;;
   esac

   # detect if dot-sourced
   eval export ${__mysourced}="NOPE"
   case "${0}" in
      "-bash"|"ksh")
         eval export ${__mysourced}="yes"
         ;;
      *)
         # cannot yet determine zsh dot-sourced script
         eval export ${__mysourced}="no"
         ;;
   esac

   # detect which file this is, exactly
   case "$( eval echo \"\$${__myshell}\" )" in
      *bash)
         eval export ${__myscript}=\"$( readlink -f "${BASH_SOURCE}" )\"
         ;;
      *ksh*)
         eval export ${__myscript}=\"$( readlink -f "${.sh.file}" )\"
         ;;
      *zsh)
         eval export ${__myscript}=\"$( readlink -f "${(%):-%x}" )\"
         ;;
      *)
         eval echo "cannot determine myshell \$${__myshell}"
         ;;
   esac

   # learn the function name
   case "$( eval echo \"\$${__myshell}\" )" in
      *bash)
         eval export ${__myfunction}=\"${FUNCNAME}\"
         ;;
      *ksh*)
         eval export ${__myfunction}=\"${.sh.fun}\"
         ;;
      *zsh)
         eval export ${__myfunction}=\"$( readlink -f "${(%):-%N}" )\"
         ;;
      *)
         eval echo "cannot determine function for shell \$${__myshell}"
         ;;
   esac

}

detect_os MYOS MYFLAVOR MYFLAVORVERSION
echo "MYOS=${MYOS}"
echo "MYFLAVOR=${MYFLAVOR}"
echo "MYFLAVORVERSION=${MYFLAVORVERSION}"

detect_shell MYSHELL MYSOURCED MYSCRIPT MYFUNCTION # assign variable MYHELL with the executable that is running this script
echo "MYSHELL=${MYSHELL}"
echo "MYSOURCED=${MYSOURCED}"
echo "MYSCRIPT=${MYSCRIPT}"
echo "MYFUNCTION=${MYFUNCTION}"
